package info.stieghorst.tasklist.jsf.model;

public enum Status {
	OPEN, FINISHED, SCHEDULED, MIGRATED;
	
	public static char printSymbol(Status stat) {
		char symbol = '?';
		if (stat == Status.OPEN) {
			symbol = '\u26aa';
		}
		else if(stat == Status.FINISHED) {
			symbol = '\u2714';
		}
		else if(stat == Status.SCHEDULED) {
			symbol = '►';
		}
		else if (stat == Status.MIGRATED) {
			symbol = '◄';
		}
		return symbol;
	}
}
