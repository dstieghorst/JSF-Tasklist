package info.stieghorst.tasklist.jsf.model;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Logger;

import info.stieghorst.tasklist.jsf.util.TaskHelper;


/**
 * The persistent class for the task database table.
 * 
 */
public class Task implements Serializable {
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(this.getClass().getName());
	
	private int id;

	private String description;

	private Status status;
	
	private LocalDate dueDate;
	
	private Date taskDate;

	private TimeUnit timeUnit;

	public Task() {
		logger.info("default constructor");
	}
	
	public Task(String description, LocalDate dueDate, TimeUnit timeUnit) {
		logger.info("constructor - 3 Argumente");
		this.description = description;
		this.dueDate = dueDate;
		this.timeUnit = timeUnit;
		this.status = Status.OPEN;
	}


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
	
	public char getStatusSymbol() {
		return Status.printSymbol(status);
	}


	public LocalDate getDueDate() {
		return dueDate;
	}


	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
		if(dueDate == null) {
			this.taskDate = null;
		}
		else {
			this.taskDate = Date.from(dueDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		}
	}
	
	public Date getTaskDate() {
		return taskDate;
	}

	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
		if(taskDate == null) {
			this.dueDate = null;
		}
		else {
			this.dueDate = Instant.ofEpochMilli(taskDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		}
	}
	
	public String getDateAsString() {
		if(dueDate == null) {
			return "";
		}
		else {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, dd.MM.yyyy");
			return dueDate.format(formatter);
		}
	}
	
	public String getTimeUnitAndDate() {
		return TaskHelper.createTitleString(dueDate, timeUnit);
	}

	public TimeUnit getTimeUnit() {
		return timeUnit;
	}


	public void setTimeUnit(TimeUnit timeUnit) {
		this.timeUnit = timeUnit;
	}
	
	public boolean isUpdateAble() {
		return this.status.equals(Status.OPEN) || this.status.equals(Status.MIGRATED);
	}

	public boolean isReadOnly() {
		return this.status.equals(Status.SCHEDULED) || this.status.equals(Status.FINISHED);
	}

	@Override
	public String toString() {
		return "Task: " + id + "|" + description + "|" + status + "|" + dueDate
				+ "|" + timeUnit;
	}
	

}