package info.stieghorst.tasklist.jsf.model;

public enum TimeUnit {
	DAY, WEEK, MONTH, YEAR, NONE;

}
