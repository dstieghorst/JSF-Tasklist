package info.stieghorst.tasklist.jsf.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import info.stieghorst.tasklist.jsf.model.Status;
import info.stieghorst.tasklist.jsf.model.TimeUnit;

public class TaskHelper {
	
	public static LocalDate calcCorrectDate(LocalDate inputDate, TimeUnit unit) {
		LocalDate outputDate = inputDate;
		int year = inputDate.getYear();
		int month = inputDate.getMonthValue();
		int dayOfWeek = inputDate.getDayOfWeek().getValue();
		
		if(unit == TimeUnit.WEEK) {
			int daysOffset = dayOfWeek - 1;
			outputDate = inputDate.minusDays(daysOffset);
		}
		else if (unit == TimeUnit.MONTH) {
			outputDate = LocalDate.of(year, month, 1);
		}
		else if(unit == TimeUnit.YEAR) {
			outputDate = LocalDate.of(year, 1, 1);
		}
		else {
			outputDate = inputDate;
		}
		
		
		return outputDate;
	}
	
	public static Date localDate2Date(LocalDate inputDate) {
		if(inputDate == null) {
			return null;
		}
		else {
			return Date.from(inputDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		}
	}
	
	public static java.sql.Date localDate2SqlDate(LocalDate inputDate) {
		if(inputDate == null) {
			return null;
		}
		else {
			Date utilDate = Date.from(inputDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
			return new java.sql.Date(utilDate.getTime());
		}
	}
	
	public static LocalDate utilDate2LocalDate(Date utilDate) {
		if(utilDate == null) {
			return null;
		}
		else {
			return Instant.ofEpochMilli(utilDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		}
	}
	
	public static TimeUnit convertTimeUnitFromString(String unitString) {
		if(unitString == null) {
			unitString = "DAY";
		}
		TimeUnit timeUnit = TimeUnit.NONE;
		switch (unitString.toUpperCase()) {
		case "DAY":
			timeUnit = TimeUnit.DAY;
			break;
		case "WEEK":
			timeUnit = TimeUnit.WEEK;
			break;
		case "MONTH":
			timeUnit = TimeUnit.MONTH;
			break;
		case "YEAR":
			timeUnit = TimeUnit.YEAR;
			break;
		default:
			timeUnit = TimeUnit.NONE;
			break;
		}
		return timeUnit;
	}
	
	public static Status convertStatusFromString(String statusString) {
		if(statusString == null) {
			statusString = "OPEN";
		}
		Status status = Status.OPEN;
		switch(statusString.toUpperCase()) {
		// OPEN, FINISHED, SCHEDULED, MIGRATED
		case "OPEN":
			status = Status.OPEN;
			break;
		case "FINISHED":
			status = Status.FINISHED;
			break;
		case "SCHEDULED":
			status = Status.SCHEDULED;
			break;
		case "MIGRATED":
			status = Status.MIGRATED;
			break;
		default:
			status = Status.OPEN;
			break;
		}
		
		return status;
	}
	
//	public static int convertMoveDirectionFromRequest(String moveFromRequest) {
//		int move = 0;
//		if(moveFromRequest == null) {
//			moveFromRequest = "";
//			move = 0;
//		}
//		else if(moveFromRequest.equals("plus")) {
//			move = 1;
//		}
//		else if(moveFromRequest.equals("minus")) {
//			move = -1;
//		}
//		return move;
//	}
	
//	public static LocalDate convertDateFromRequest(String dateFromRequest) {
//		LocalDate selectedDate = LocalDate.now();
//		if(dateFromRequest != null) {
//			int year = Integer.parseInt(dateFromRequest.substring(0,  4));
//			int month = Integer.parseInt(dateFromRequest.substring(5, 7));
//			int day = Integer.parseInt(dateFromRequest.substring(8, 10));
//			selectedDate = LocalDate.of(year, month, day);
//		}
//		return selectedDate;
//	}
	
	public static LocalDate calcDateForRequestedPage(LocalDate currentDate, int move, TimeUnit timeUnit) {
		LocalDate newDate = currentDate;
		
		if (timeUnit == TimeUnit.DAY) {
			newDate = TaskHelper.calcCorrectDate(currentDate, timeUnit).plusDays(move);
		} else if (timeUnit == TimeUnit.WEEK) {
			newDate = TaskHelper.calcCorrectDate(currentDate, timeUnit).plusWeeks(move);
		} else if (timeUnit == TimeUnit.MONTH) {
			newDate = TaskHelper.calcCorrectDate(currentDate, timeUnit).plusMonths(move);
		} else if (timeUnit == TimeUnit.YEAR) {
			newDate = TaskHelper.calcCorrectDate(currentDate, timeUnit).plusYears(move);
		} else {
			newDate = currentDate;
		}
		
		return newDate;
	}
	
	public static String createTitleString(LocalDate date, TimeUnit timeUnit) {
		String title = "";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, dd.MM.yyyy");
		
		if (timeUnit == TimeUnit.DAY) {
			formatter = DateTimeFormatter.ofPattern("EEE, dd.MM.yyyy");
			title = date.format(formatter);
			if(date.equals(LocalDate.now())) {
				title = title + " (heute)";
			}
		} else if (timeUnit == TimeUnit.WEEK) {
			formatter = DateTimeFormatter.ofPattern("w/yyyy (dd.MM.");
			title = "KW " + date.format(formatter);
			formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy)");
			title = title + " - " + date.plusDays(6).format(formatter);
		} else if (timeUnit == TimeUnit.MONTH) {
			formatter = DateTimeFormatter.ofPattern("MMMM yyyy");
			title = date.format(formatter);
		} else if (timeUnit == TimeUnit.YEAR) {
			formatter = DateTimeFormatter.ofPattern("yyyy");
			title = date.format(formatter);
		} else if(timeUnit == TimeUnit.NONE) {
			title = "(ohne Datum)";
		}
		
		return title;
	}

}
