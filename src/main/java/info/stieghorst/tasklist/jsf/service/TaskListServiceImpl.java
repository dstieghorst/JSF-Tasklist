package info.stieghorst.tasklist.jsf.service;

import java.time.LocalDate;
import java.util.List;
import java.util.logging.Logger;

import info.stieghorst.tasklist.jsf.dao.JdbcTaskListDAOImpl;
import info.stieghorst.tasklist.jsf.dao.TaskListDAO;
import info.stieghorst.tasklist.jsf.model.Status;
import info.stieghorst.tasklist.jsf.model.Task;
import info.stieghorst.tasklist.jsf.model.TimeUnit;

public class TaskListServiceImpl implements TaskListService {
	
	Logger logger = Logger.getLogger("TaskListServiceImpl");
	
	private TaskListDAO dao;
	
	public TaskListServiceImpl() throws Exception {
		dao = JdbcTaskListDAOImpl.getInstance();
	}

	@Override
	public List<Task> getTaskList(LocalDate date, TimeUnit timeUnit) throws Exception {
		return dao.getTaskList(date, timeUnit);
	}

	@Override
	public boolean addTask(Task newTask) throws Exception {
		if(newTask.getTimeUnit().equals(TimeUnit.NONE)) {
			newTask.setDueDate(null);
		}
		return dao.addTask(newTask);
	}

	@Override
	public boolean updateTask(Task updatedTask) throws Exception {
		if(updatedTask.getTimeUnit().equals(TimeUnit.NONE)) {
			updatedTask.setDueDate(null);
		}
		return dao.updateTask(updatedTask);
	}

	@Override
	public boolean postponeTask(int id) throws Exception {
		logger.info("postponeTask(" + id + ")");
		Task originalTask = dao.getTaskById(id);
		TimeUnit timeUnit = originalTask.getTimeUnit();
		LocalDate dueDate = originalTask.getDueDate();

		Task newTask = new Task();
		newTask.setDescription(originalTask.getDescription());
		newTask.setStatus(Status.MIGRATED);
		newTask.setTimeUnit(originalTask.getTimeUnit());
		
		if(timeUnit.equals(TimeUnit.DAY)) {
			dueDate = dueDate.plusDays(1);
		}
		else if(timeUnit.equals(TimeUnit.WEEK)) {
			dueDate = dueDate.plusWeeks(1);
		}
		else if(timeUnit.equals(TimeUnit.MONTH)) {
			dueDate = dueDate.plusMonths(1);
		}
		else if(timeUnit.equals(TimeUnit.YEAR)) {
			dueDate = dueDate.plusYears(1);
		}
		newTask.setDueDate(dueDate);
		
		dao.addTask(newTask);
		
		originalTask.setStatus(Status.SCHEDULED);

		return dao.updateTask(originalTask);
	}

	@Override
	public Task getTaskById(int id) throws Exception {
		logger.info("getTaskById(" + id + ")");
		
		Task tempTask = dao.getTaskById(id);
		if(tempTask == null) {
			logger.info("DAO returned NULL!");
		}
		else {
			logger.info("Task gefunden:\n" + tempTask);
		}
		
		return tempTask;
	}

	@Override
	public boolean finishTask(int id) throws Exception {
		logger.info("finishTask(" + id + ")");
		Task originalTask = dao.getTaskById(id);
		originalTask.setStatus(Status.FINISHED);
		return dao.updateTask(originalTask);
		
	}
	
	@Override
	public boolean createTaskTable() throws Exception {
		logger.info("createTaskTable");
		return dao.createTaskTable();
	}

}
