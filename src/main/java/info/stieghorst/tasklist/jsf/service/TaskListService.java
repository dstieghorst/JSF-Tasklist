package info.stieghorst.tasklist.jsf.service;

import java.time.LocalDate;
import java.util.List;

import info.stieghorst.tasklist.jsf.model.Task;
import info.stieghorst.tasklist.jsf.model.TimeUnit;

public interface TaskListService {
	
	List<Task> getTaskList(LocalDate date, TimeUnit timeUnit) throws Exception;
	
	boolean addTask(Task newTask) throws Exception;
	
	boolean postponeTask(int id) throws Exception;
	
	boolean updateTask(Task updatedTask) throws Exception;
	
	Task getTaskById(int id) throws Exception;

	boolean finishTask(int id) throws Exception;
	
	boolean createTaskTable() throws Exception;

}
