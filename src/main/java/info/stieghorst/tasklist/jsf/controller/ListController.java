package info.stieghorst.tasklist.jsf.controller;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import info.stieghorst.tasklist.jsf.model.Status;
import info.stieghorst.tasklist.jsf.model.Task;
import info.stieghorst.tasklist.jsf.model.TimeUnit;
import info.stieghorst.tasklist.jsf.service.TaskListService;
import info.stieghorst.tasklist.jsf.service.TaskListServiceImpl;
import info.stieghorst.tasklist.jsf.util.TaskHelper;

@ManagedBean
@SessionScoped
public class ListController {
	
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	private LocalDate selectedDate;
	private TimeUnit timeUnit;
	
	private Map<String, TimeUnit> timeUnitMap;
	
	private Map<Character, Status> statusMap;
	
	private TaskListService service;
	
	private List<Task> currentTaskList;
	
	private Task currentTask;
	
	private String errorMessage;
	
	public ListController() throws Exception {
		service = new TaskListServiceImpl();

		selectedDate = LocalDate.now();
		timeUnit = TimeUnit.DAY;
		
		timeUnitMap = new LinkedHashMap<>();
		timeUnitMap.put("Tag", TimeUnit.DAY);
		timeUnitMap.put("Woche", TimeUnit.WEEK);
		timeUnitMap.put("Monat", TimeUnit.MONTH);
		timeUnitMap.put("Jahr", TimeUnit.YEAR);
		timeUnitMap.put("-ohne-", TimeUnit.NONE);
		
		statusMap = new LinkedHashMap<>();
		statusMap.put(Status.printSymbol(Status.OPEN), Status.OPEN);
		statusMap.put(Status.printSymbol(Status.FINISHED), Status.FINISHED);
		statusMap.put(Status.printSymbol(Status.SCHEDULED), Status.SCHEDULED);
		statusMap.put(Status.printSymbol(Status.MIGRATED), Status.MIGRATED);
	}
	
	public String getDateTitle() {
		return TaskHelper.createTitleString(selectedDate, timeUnit);
	}

	public LocalDate getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(LocalDate selectedDate) {
		this.selectedDate = selectedDate;
	}

	public TimeUnit getTimeUnit() {
		return timeUnit;
	}

	public void setTimeUnit(TimeUnit timeUnit) {
		this.timeUnit = timeUnit;
	}
	
	public List<Task> getCurrentTaskList() {
		return currentTaskList;
	}
	
	public Task getCurrentTask() {
		return currentTask;
	}

	public void setCurrentTask(Task currentTask) {
		this.currentTask = currentTask;
	}

	public Map<String, TimeUnit> getTimeUnitMap() {
		return timeUnitMap;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void loadTasklist() throws Exception {
		logger.info("loadTasklist()");
		currentTaskList = service.getTaskList(selectedDate, timeUnit);
	}

	public String newListForTimeUnit(String input) {
		logger.info("newListForTimeUnit(" + input + ")");
		timeUnit = TaskHelper.convertTimeUnitFromString(input);
		selectedDate = LocalDate.now();
		selectedDate = TaskHelper.calcCorrectDate(selectedDate, timeUnit);

		try {
			loadTasklist();
		} catch (Exception e) {
			errorMessage = e.getLocalizedMessage();
			return "error";
		}
		
		return "tasklist?faces-redirect=true";
	}
	
	public String newListForMove(String input) {
		logger.info("newListForMove(" + input + ")");
		int move = 0;
		if(input.equals("next")) {
			move = 1;
		}
		else if(input.equals("previous")) {
			move = -1;
		}
		else {
			move = 0;
		}
		selectedDate = TaskHelper.calcDateForRequestedPage(selectedDate, move, timeUnit);
		logger.info("newListForMove() - selectedDate = " + selectedDate);
		
		try {
			loadTasklist();
		} catch (Exception e) {
			errorMessage = e.getLocalizedMessage();
			return "error";
		}
		
		return "tasklist?faces-redirect=true";
	}
	
	public String showNewTaskForm() {
		logger.info("showNewTaskForm()");
		currentTask = new Task();
		currentTask.setDueDate(LocalDate.now());
		currentTask.setTimeUnit(TimeUnit.NONE);
		currentTask.setStatus(Status.OPEN);
		return "add-task-form";
	}
	
	public String addNewTask() {
		logger.info("addNewTask()");
		LocalDate correctedDate = TaskHelper.calcCorrectDate(
				currentTask.getDueDate(), currentTask.getTimeUnit());
		currentTask.setDueDate(correctedDate);

		try {
			service.addTask(currentTask);
		} catch (Exception e) {
			errorMessage = e.getLocalizedMessage();
			return "error";
		}
		
		return "tasklist?faces-redirect=true";
	}
	
	public String showUpdateForm(int id) {
		logger.info("showUpdateForm(" + id + ")");
		Task tempTask = null;

		try {
			tempTask = service.getTaskById(id);
		} catch (Exception e) {
			errorMessage = e.getLocalizedMessage();
			return "error";
		}
		
		if(tempTask == null) {
			logger.info("getTaskByID returned NULL!!!");
		}
		else {
			currentTask = tempTask;
			logger.info("currentTask: \n" + currentTask);
		}
		
		return "update-task-form";
	}
	
	public String updateTask() {
		logger.info("updateTask()");

		try {
			service.updateTask(currentTask);
		} catch (Exception e) {
			errorMessage = e.getLocalizedMessage();
			return "error";
		}
		
		return "tasklist?faces-redirect=true";
	}
	
	public String postponeTask(int id) {
		logger.info("postponeTask(" + id + ")");

		try {
			service.postponeTask(id);
		} catch (Exception e) {
			errorMessage = e.getLocalizedMessage();
			return "error";
		}
		
		return "tasklist?faces-redirect=true";
		
	}
	
	public String finishTask(int id) {
		logger.info("finishTask(" + id + ")");

		try {
			service.finishTask(id);
		} catch (Exception e) {
			errorMessage = e.getLocalizedMessage();
			return "error";
		}
		
		return "tasklist?faces-redirect=true";
	}

}
