package info.stieghorst.tasklist.jsf.config;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import info.stieghorst.tasklist.jsf.service.TaskListService;
import info.stieghorst.tasklist.jsf.service.TaskListServiceImpl;

@ManagedBean(eager=true)
@ApplicationScoped
public class DatabaseConfig {
	
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	private TaskListService service;
	
	public DatabaseConfig() throws Exception {
		service = new TaskListServiceImpl();
	}

	@PostConstruct
    public void init() {
        logger.info("init method called ...");
        try {
            boolean success = service.createTaskTable();
            logger.info("create table task ended successfully: " + success);
        } catch(Exception e) {
        	logger.severe("exception when creating table: " + e.getMessage());
        }
    }

    @PreDestroy
    public void destroy() {
        logger.info("destroy method called ...");
    }
	

}
