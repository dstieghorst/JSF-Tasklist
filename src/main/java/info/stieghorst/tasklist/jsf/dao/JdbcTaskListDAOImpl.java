package info.stieghorst.tasklist.jsf.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import info.stieghorst.tasklist.jsf.model.Task;
import info.stieghorst.tasklist.jsf.model.TimeUnit;
import info.stieghorst.tasklist.jsf.util.TaskHelper;

public class JdbcTaskListDAOImpl implements TaskListDAO {
	
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	private static JdbcTaskListDAOImpl instance;
//	private DataSource dataSource;
//	private String jndiName = "java:comp/env/jdbc/tasklist"; 
	
	private JdbcTaskListDAOImpl() throws Exception {
//		dataSource = getDataSource();
	}
	
//	private DataSource getDataSource() throws NamingException {
//		Context context = new InitialContext();
//		DataSource theDataSource = (DataSource) context.lookup(jndiName);
//		return theDataSource;
//	}
	
	public static JdbcTaskListDAOImpl getInstance() throws Exception {
		if(instance == null) {
			instance = new JdbcTaskListDAOImpl();
		}
		return instance;
	}
		


	@Override
	public List<Task> getTaskList(LocalDate date, TimeUnit timeUnit) throws Exception {
		logger.info("getTaskList"
				+ "\n-- date: " + date
				+ "\n-- timeUnit: " + timeUnit);
		
		List<Task> taskList = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String sql = "";
		
		if(timeUnit.equals(TimeUnit.NONE)) {
			sql = "select * from task where timeunit = ? order by id";
		}
		else {
			sql = "select * from task "
					+ "where (due = ? and timeunit = ?) "
					+ "or (due <= ? and timeunit != ? and (status = 'OPEN' or status = 'MIGRATED')) "
					+ "order by id";
		}
		
		Date sqlDate = TaskHelper.localDate2SqlDate(date);
		
		try {
			connection = getConnection();
			statement = connection.prepareStatement(sql);
			if(timeUnit.equals(TimeUnit.NONE)) {
				statement.setString(1, timeUnit.name());
			}
			else {
				statement.setDate(1, sqlDate);
				statement.setString(2, timeUnit.name());
				statement.setDate(3, sqlDate);
				statement.setString(4, timeUnit.name());
			}

			resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				int id = resultSet.getInt("id");
				String descr = resultSet.getString("descr");
				String status = resultSet.getString("status");
				String timeunit = resultSet.getString("timeunit");
				Date due = resultSet.getDate("due");

				Task nextTask = new Task();
				nextTask.setId(id);
				nextTask.setDescription(descr);
				nextTask.setStatus(TaskHelper.convertStatusFromString(status));
				nextTask.setTimeUnit(TaskHelper.convertTimeUnitFromString(timeunit));
				nextTask.setDueDate(TaskHelper.utilDate2LocalDate(due));
				
				taskList.add(nextTask);
			}
			
			
			return taskList;
		} finally {
			close(connection, statement, resultSet);
		}
	}

	@Override
	public Task getTaskById(int id) throws Exception {
		logger.info("getTaskById" 
				+ "\n-- id: " + id);
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		String sql = "select * from task where id = ?";
		
		Task task = null;
		
		try {
			connection = getConnection();
			statement = connection.prepareStatement(sql);
			
			statement.setInt(1, id);
			
			resultSet = statement.executeQuery();
			
			if(resultSet.next()) {
				int idFromDb = resultSet.getInt("id");
				String descr = resultSet.getString("descr");
				String status = resultSet.getString("status");
				String timeunit = resultSet.getString("timeunit");
				Date due = resultSet.getDate("due");

				task = new Task();
				task.setId(idFromDb);
				task.setDescription(descr);
				task.setStatus(TaskHelper.convertStatusFromString(status));
				task.setTimeUnit(TaskHelper.convertTimeUnitFromString(timeunit));
				task.setDueDate(TaskHelper.utilDate2LocalDate(due));
				
				logger.info("Task found:"
						+ "\n-- " + task);
			}
			
		} finally {
			close(connection, statement, resultSet);
		}
		return task;
	}	

	@Override
	public boolean addTask(Task newTask) throws Exception {
		logger.info("addTask");
		logger.info("-- newTask: " + newTask);
		
		Connection connection = null;
		PreparedStatement statement = null;
		
		String sql = "insert into task(descr, status, timeunit, due) values(?, ?, ?, ?)";
		
		try {
			connection = getConnection();
			statement = connection.prepareStatement(sql);
			
			statement.setString(1, newTask.getDescription());
			statement.setString(2, newTask.getStatus().name());
			statement.setString(3, newTask.getTimeUnit().name());
			Date sqlDate = TaskHelper.localDate2SqlDate(newTask.getDueDate());
			statement.setDate(4, sqlDate);
			
			return statement.execute();
		} finally {
			close(connection, statement	);
		}
	}

	@Override
	public boolean updateTask(Task updatedTask) throws Exception {
		logger.info("updateTask"
				+ "\n-- updatedTask: " + updatedTask);
		
		Connection connection = null;
		PreparedStatement statement = null;
		
		String sql = "update task set descr = ?, status = ?, timeunit = ?, due = ? where id = ?";
		
		try {
			connection = getConnection();
			statement = connection.prepareStatement(sql);
			
			statement.setString(1, updatedTask.getDescription());
			statement.setString(2, updatedTask.getStatus().name());
			statement.setString(3, updatedTask.getTimeUnit().name());
			Date sqlDate = TaskHelper.localDate2SqlDate(updatedTask.getDueDate());
			statement.setDate(4, sqlDate);
			statement.setInt(5, updatedTask.getId());
			
			return statement.execute();
		} finally {
			close(connection, statement	);
		}
	}
	
	@Override
	public boolean createTaskTable() throws Exception {
		logger.info("createTaskTable");
		
		boolean success = false;
		Connection connection = null;
		Statement statement = null;
		
		String sql = "CREATE TABLE TASK (" + 
				"  ID int(11) NOT NULL AUTO_INCREMENT," + 
				"  DESCR varchar(60) NOT NULL," + 
				"  STATUS varchar(10) NOT NULL," + 
				"  TIMEUNIT varchar(10) NOT NULL," + 
				"  DUE date DEFAULT NULL," + 
				"  PRIMARY KEY (ID)," + 
				"  UNIQUE KEY UNIQ (DESCR,STATUS,TIMEUNIT,DUE))";
		try {
			connection = getConnection();
			statement = connection.createStatement();
			
			success =  statement.execute(sql);
		} finally {
			close(connection, statement	);
		}
		return success;
	}

	private Connection getConnection() throws Exception {
//		Connection connection = dataSource.getConnection();
//		return connection;
		Connection connection = DriverManager.getConnection("jdbc:h2:~/task", "sa", "");
		return connection;
	}
	
	private void close(Connection connection, Statement statement) {
		close(connection, statement, null);
	}
	
	private void close(Connection connection, Statement statement, ResultSet resultSet) {

		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connection != null) {
				connection.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}


