# JSF-Tasklist

Eine einfache Todo-Liste inspiriert vom [Bullet Point Journal](http://bulletjournal.com/)

Aufgaben können einem Tag, einer Woche usw. zugeordnet werden oder auch keinen festen Termin haben

Neben dem Termin hat jede Aufgabe einen Status: 

* offen (im Bullet Point Journal mit einem Punkt in der Mitte der Zeile gekennzeichnet: **&sdot;**. Dieser kann leicht mit den anderen Symbolen überschrieben werden)
* erledigt (im BPJ: X, bleibt damit weiterhin sichtbar)
* verschoben auf später (BPJ: >, in der bisherigen Zeiteinheit sichtbar)
* verschoben von einem früheren Termin (BPJ: <)

Standard-Ansicht ist der heutige Tag. Es kann auf die anderen Zeiteinheiten umgeschaltet und jeweils um eine Einheit vor und zurück geblättert werden.

---

## Implementation

+ JSF 2.2
+ PrimeFaces 6.2
+ H2 Database
+ Maven mit Jetty-Plugin

